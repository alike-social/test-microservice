from flask import Flask, jsonify, request
from pymongo import MongoClient

client = MongoClient('mongodb:27017')
app = Flask(__name__)

@app.route('/user', methods=['POST'])
def add_user():
    new_user = {
        "name": request.json['name'],
        "password": request.json['password']
    }

    with MongoClient('mongodb:27017') as client:
        db = client.users
        db.user.insert_one(new_user);
        return jsonify({"message":"Usuario  añadido"})

@app.route('/user', methods=['GET'])
def check_user():
    user = {
        "name": request.json['name'],
        "password": request.json['password']
    }

    with MongoClient('mongodb:27017') as client:
        db = client.users
        query = {"name":user['name']
                         , "password": user['password']}

        if db.user.find_one(query):
            return jsonify({"message":"authenticado"})

        else:
            return jsonify({"message":"usuario o contraseña incorrectos"})

@app.route('/user', methods=['PUT'])
def edit_user():
    user = {
        "name": request.json['name'],
        "old_password": request.json['old_password'],
        "new_password": request.json['new_password']
    }
    
    with MongoClient('mongodb:27017') as client:
        db = client.users
        query = {"name":user['name']
                         , "password": user['old_password']}
        
        if db.user.find_one(query):
            update_query = {"$set": {"name":user['name']
                                     , "password": user['new_password']}}
            db.user.update_one(query, update_query)
            return jsonify({"message":"usuario actualizado"})

        else:
            return jsonify({"message":"usuario o contraseña incorrectos"})


@app.route('/user', methods=['DELETE'])
def delete_user():

     user = {
         "name": request.json['name'],
         "password": request.json['password']
     }
     
     with MongoClient('mongodb:27017') as client:
        db = client.users
        query = {"name":user['name'], "password": user['password']}
        
        if db.user.find_one(query):
            db.user.delete_one(query)
            return jsonify({"message":"usuario eliminado"})

        else:
            return jsonify({"message":"usuario o contraseña incorrectos"})

if __name__ =='__main__':  
    app.run(host='0.0.0.0', debug = True)  
