from flask import Flask, jsonify, request

app = Flask(__name__)

pokedex = []

@app.route('/')
def hello():
    return jsonify({"message":"Hola caracola!"})


@app.route('/pokemon', methods=['GET'])
def indexPokemon():
    return jsonify(pokedex)

@app.route('/pokemon', methods=['POST'])
def addPokemon():
    nuevo_pokemon = {
        "name": request.json['name'],
        "type": request.json['type']
    }
    pokedex.append(nuevo_pokemon)
    return jsonify({"message":"Pokemon añadido"})

@app.route('/pokemon/<string:pokemon_name>', methods=['PUT'])
def editPokemon(pokemon_name):
    pokemonFound = [pokemon for pokemon in pokedex if pokemon['name'] == pokemon_name]
    print(len(pokemonFound))
    if(len(pokemonFound)>0):
        print("found it!")
        pokemonFound[0]['name']= request.json['name']
        pokemonFound[0]['type']= request.json['type']
        return jsonify({"message":"Pokemon editado"})
    else:
        return jsonify({"message":"Pokemon no encontrado"})


@app.route('/pokemon/<string:pokemon_name>', methods=['DELETE'])
def deletePokemon(pokemon_name):
    pokemonFound = [pokedex.remove(pokemon) for pokemon in pokedex if pokemon['name'] == pokemon_name]
    print(len(pokemonFound))
    if(len(pokemonFound)>0):
        return jsonify({"message":"Pokemon eliminado"})
    else:
        return jsonify({"message":"Pokemon no encontrado"})


if __name__ =='__main__':  
    app.run(host='0.0.0.0', debug = True)  
