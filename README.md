# test-microservice

Test para probar microservicios con flask.

## Instalación y configuración del entorno virtual
Clonaremos este repositorio:
```bash
#HTTPS
git clone https://gitlab.com/alike-social/test-microservice

#SSH (recomendado)
#Generamos la clave
ssh-keygen -t rsa -b 4096
cat ~/.ssh/id_rsa #La añadimos en gitlab en editar perfil/ssh keys
#Clonamos el repositorio
git clone git@gitlab.com:alike-social/test-microservice.git
```
En algunos sistemas como Ubuntu, puede ser necesario la instalación de venv:  
`
sudo apt install python3.8-venv
`
python3 -m venv nombre_entorno
`

Para activar el entorno:  
`
source /nombre_entorno/bin/activate
`

En Windows:  
`
nombre_entorno\Scripts\activate.bat
`

Una vez en el entorno instalaremos las dependencias con el comando:  
`
pip install -r requirements.txt  
`

Ejecutaremos la app:
` 
python3 basicExample/app.py
`
y visitaremos `localhost:5000` para comprobar que funciona.  

Para salir del entorno virtual:
`
deactivate
`
## Test de la CRUD API
Para testear la API primero importa las llamadas HTTP en el cliente REST Insomnia (basicExample/basic-examples-calls.json).  
### Añadir
Ejecuta la llamada addPokemon.  
URL: `localhost:5000/pokemon`  
BODY:
```json
{
	"name": "pikachu",
	"type": "electrico"
}
```
### Mostrar
Ejecuta la llamada indexPokemon.   
URL: `localhost:5000/pokemon`
### Editar
Ejecuta la llamada editPokemon  
URL: `localhost:5000/pokemon/pokemon_name`  
BODY:
```json
{
	"name": "raichu",
	"type": "electrico"
}
```
### Eliminar
Ejecuta la llamada deletePokemon.  
URL: `localhost:5000/pokemon/pokemon_name`


